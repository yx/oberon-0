#pragma once

#define ID_LENGTH 32 // for ease limit identifiers in length

#define TOK_NULL 0
#define TOK_TIMES 1
#define TOK_DIV 2
#define TOK_MOD 3
#define TOK_AND 4
#define TOK_PLUS 5
#define TOK_MINUS 6
#define TOK_OR 7
#define TOK_EQUAL 8
#define TOK_NOTEQUAL 9
#define TOK_LESSTHAN 10
#define TOK_LESSTHANEQUAL 11
#define TOK_GREATERTHAN 12
#define TOK_GREATERTHANEQUAL 13
#define TOK_PERIOD 14
#define TOK_COMMA 15
#define TOK_COLON 16
#define TOK_RIGHTPAREN 17
#define TOK_RIGHTBRACKET 18
#define TOK_LEFTPAREN 19
#define TOK_LEFTBRACKET 20
#define TOK_NOT 21
#define TOK_ASSIGN 22
#define TOK_SEMICOLON 23
#define TOK_OF 24
#define TOK_THEN 25
#define TOK_DO 26
#define TOK_UNTIL 27
#define TOK_END 28
#define TOK_ELSE 29
#define TOK_ELSIF 30
#define TOK_IF 31
#define TOK_WHILE 32
#define TOK_REPEAT 33
#define TOK_ARRAY 34
#define TOK_RECORD 35
#define TOK_CONSTANT 36
#define TOK_TYPE 37
#define TOK_VARIABLE 38
#define TOK_PROCEDURE 39
#define TOK_BEGIN 40
#define TOK_MODULE 41
#define TOK_IDENTIFIER 42
#define TOK_INTEGER 43
#define TOK_FALSE 44
#define TOK_TRUE 45

extern char ident[ID_LENGTH];
extern int val;
extern size_t line;

extern char *tokens[];

extern FILE *source;

int scanToken(); // grab next token from input and return it

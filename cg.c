#include <stdarg.h>
#include <stdio.h>

#include "cg.h"
#include "error.h"
#include "store.h"

FILE *outc;

void
setupOutput(const char *name)
{
	outc = fopen(name, "w");
	if (outc == NULL) error("couldn't open file: %s", name);
}

void
emit(const char *str, ...)
{
	va_list args;
	va_start(args, str);
	vfprintf(outc, str, args);
	va_end(args);
}

void
emitType(struct Type *t)
{
	struct Object *current; // for records
	switch (t->form) {
	case TYPE_INTEGER:
		emit("int");
		break;
	case TYPE_BOOLEAN:
		emit("char"); // treat bools as char types
		break;
	case TYPE_ARRAY:
		emitType(t->base);
		// after this is emitted, the [%i] is emitted by outside function
		break;
	case TYPE_RECORD:
		emit("struct{");
		current = t->fields;
		while (current != NULL) {
			emitType(current->type);
			emit(" %s", current->name);
			if (current->type->form == TYPE_ARRAY) emit("[%i]", current->type->len);
			emit(";");
			current = current->previous;
		}
		emit("}");
		break;
	default: break; // unreachable
	}
}

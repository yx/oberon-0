#pragma once
#include "scan.h"

// things that identifiers can be
#define CLASS_CONSTANT 0
#define CLASS_TYPE 1
#define CLASS_VARIABLE 2
#define CLASS_PROCEDURE 3
#define CLASS_PARAMETER 4
#define CLASS_CONST_PARAM 5

// things that types can be
#define TYPE_INTEGER 0
#define TYPE_BOOLEAN 1
#define TYPE_ARRAY 2
#define TYPE_RECORD 3

struct Type;

// expression evaluation helpers
struct Exp {
	struct Type *type;
	int value;
	int class;
};

struct Object {
	char name[ID_LENGTH];
	int class;
	int val; // store booleans as integers, it'll work fine
	struct Type *type;
	struct Object *params; // for procedures
	struct Object *previous; // reverse link the list
};

struct Type {
	int form;
	struct Object *fields; // used for record types
	int len; // used for array types
	struct Type *base; // used for array types
};

struct Scope {
	char name[ID_LENGTH];
	struct Object *last; // each scope gets objects
	struct Scope *previous; // previous scope
};

struct TypeCollection {
	struct Type *type;
	struct TypeCollection *previous;
};

extern struct Scope *lastScope; // will be the furthest scope down

extern struct TypeCollection *typeRoot; // keep track of all type bases
extern struct Type *baseInt, *baseBool; // shortcuts for literals

void expNeg(struct Exp *);
void expCombine(int, struct Exp *, struct Exp *);

void destroyAllScopes();
void destroyScope();
void newScope(const char *);
void newObject(const char *, int);
void newObjectAttached(struct Object **, const char *, int);
void newField(struct Object **, const char *, int);
struct Object *findObject(const char *);
void initTopScope(); // create primitive identifiers

void addType(int, struct Object *, int, struct Type *);
void destroyTypeCollection();

void printScopes();

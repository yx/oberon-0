# Oberon-0 compiler
Following Wirth's Compiler Construction 2017 revision to learn how to
create a compiler. The plan is to write a compiler in C that compiles to C99
in a portable way. It's not quite machine code generation, but I can learn that
when I implement my own language. Everything else for certain will be useful to
me in compiler design. The compiler scans keywords, identifiers, and integers
into returnable tokens, that the compiler can then use to check for syntactic
errors via recursive descent parsing.

## Current Status
The compiler is able to scan a source file, produce symbols, and parse
symbols according to the Oberon-0 grammar. It is listed in `syntax.ebnf`
Now it can also type check, and generate code from this grammar. It is very
buggy and I'm sure there are bugs I haven't seen. However, this project has
served its purpose in teaching me about compiler design and implementation.

## Conclusion
Compilers are *messy*. Even with a "simple" recursive descent parser it quickly
becomes difficult to understand the flow of the parser and subsequently the
code generator. I definitely see the appeal of concatenative languages like
Forth, Joy, and Factor much more. Additionally, I learned that C is not a good
language for implementing a compiler. Many features that would have made this
process easier (Sum types, pattern matching, etc.) were not present in C. I
wouldn't even consider these high level features. Additionally, C is not a
great compilation backend for a language like this. Nesting procedures is an
explicitly allowed part of Oberon-0, and C really struggles to implement this
simply. The problem arose with environment capture, and I ultimately didn't
implement a solution. This would be almost trivial to solve for a native code
backend, but C is rather limited in this regard. Ultimately, I think I will
focus much more attention on Concatenative programming languages in the future.
If this is what a "simple" compiler looks like I'm terrified of what a "real"
compiler would look like. This code is <1500 lines and I can barely keep track
of the internal state. I would not want to work on something like the Rust
borrow checker. The syntax and execution semantics of a concatenative language
are ultimately much simpler and cleaner to understand/implement.

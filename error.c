#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

#include "cg.h"
#include "error.h"

static char buffer[1024]; // unlikely our error message will be this long

extern size_t line;

void
error(const char *err, ...)
{
	va_list args;
	va_start(args, err);
	fprintf(stderr, "error line %lu: ", line);
	vfprintf(stderr, err, args);
	va_end(args);
	printf("\n");
	exit(1);
}

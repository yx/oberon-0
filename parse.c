#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "cg.h"
#include "error.h"
#include "scan.h"
#include "store.h"
#include "parse.h"


static int token;

// parse helper functions
inline void
next()
{
	token = scanToken();
}

inline void
expect(int tok)
{
	if (token != tok)
		error("expected '%s' but got '%s'", tokens[tok], tokens[token]);
	next();
}

inline void
expectIdentifier()
{
	if (token != TOK_IDENTIFIER) error("expected identifier");
	next();
}

// type checking functions
inline void
checkInt(struct Exp *e)
{
	if (e->type != TYPE_INTEGER) error("not an integer");
}

/* begin copius amounts of global scope functions
 * why can't c have nested procedures
 */

void
parse()
{
	emit("#include <stdio.h>\nvoid WriteInt(int N){printf(\"%%i\\n\",N);}");
	next();
	module();
}

void
module()
{
	expect(TOK_MODULE);
	newScope(ident); // if we don't have an identifier the program aborts anyway
	expectIdentifier();
	expect(TOK_SEMICOLON);
	declarations();
	if (token == TOK_BEGIN) {
		emit("int main(int argc,char **argv){");
		next();
		statementSequence();
		emit("return 0;}");
	}
	expect(TOK_END);
	expectIdentifier();
	expect(TOK_PERIOD);
}

void
declarations()
{
	if (token == TOK_CONSTANT) {
		next();
		while (token == TOK_IDENTIFIER) {
			newObject(ident, CLASS_CONSTANT);
			emit("const "); // types will only be bool or int
			next();
			expect(TOK_EQUAL);
			struct Exp x = expression(0);
			if (x.class != CLASS_CONSTANT) error("expected constant expression");
			lastScope->last->val = x.value;
			lastScope->last->type = x.type;
			emitType(x.type);
			emit(" %s=%i;", lastScope->last->name, x.value);
			expect(TOK_SEMICOLON);
		}
	}
	/*
		we actually don't need to emit code for typedefs
		the compiler automatically inserts the type definitions in their
		entirety when compiling
	*/
	if (token == TOK_TYPE) {
		next();
		while (token == TOK_IDENTIFIER) {
			next();
			newObject(ident, CLASS_TYPE);
			expect(TOK_EQUAL);
			struct Type *t = type();
			lastScope->last->type = t;
			expect(TOK_SEMICOLON);
		}
	}
	if (token == TOK_VARIABLE) {
		next();
		while (token == TOK_IDENTIFIER) { // this adds identifiers to the list
			identList(&lastScope->last, CLASS_VARIABLE);
			expect(TOK_COLON);
			struct Type *t = type(); // now we should check for type correctness
			// walk up the current scope and fill in any unassigned variable types
			struct Object *current = lastScope->last;
			while (current != NULL && current->type == NULL) {
				current->type = t;
				emitType(t);
				emit(" %s", current->name);
				if (t->form == TYPE_ARRAY) emit("[%i]", t->len);
				emit(";");
				current = current->previous;
			}
			expect(TOK_SEMICOLON);
		}
	}
	while (token == TOK_PROCEDURE) { // ProcedureDeclaration
		next(); // ProcedureHeading
		newObject(ident, CLASS_PROCEDURE);
		newScope(ident);
		expectIdentifier();
		if (token == TOK_LEFTPAREN) {
			next();
			if (token == TOK_VARIABLE || token == TOK_IDENTIFIER) { //FPSection check
				struct Object *param = fpSection();
				while (token == TOK_SEMICOLON) {
					next();
					struct Object *param2 = fpSection();
					struct Object *current = param2;
					while (current != NULL) {
						struct Object *next = current->previous;
						current->previous = param;
						param = current;
						current = next;
					}
				}
				lastScope->previous->last->params = param;
			}
			expect(TOK_RIGHTPAREN);
		}
		expect(TOK_SEMICOLON);
		declarations(); // ProcedureBody
		if (token == TOK_BEGIN) {
			emit("void %s(", lastScope->name);
			struct Object *current = lastScope->previous->last->params;
			while (current != NULL && current->class < CLASS_PARAMETER)
				current = current->previous;
			while (current != NULL) { // now we have the parameters we need
				emitType(current->type);
				if (current->class == CLASS_PARAMETER) emit("*");
				emit(" %s", current->name);
				if (current->type->form == TYPE_ARRAY)
					emit("[%i]", current->type->len);
				if (current->previous == NULL) break;
				emit(",");
				current = current->previous;
			}
			emit("){");
			next();
			statementSequence();
			emit("}");
		}
		expect(TOK_END);
		expectIdentifier();
		expect(TOK_SEMICOLON);
		destroyScope(); // we no longer need it
	}
}

const char *opEmit[] = {"*", "/", "%", "&&", "+", "-", "||", "==", "!=", "<", "<=", ">", ">=" };

struct Exp
expression(char emitFlag)
{
	if (emitFlag) emit("(");
	struct Exp x = simpleExpression(emitFlag);
	if (token >= TOK_EQUAL && token <= TOK_GREATERTHANEQUAL) {
		int op = token - TOK_TIMES;
		if (emitFlag) emit("%s", opEmit[op]);
		next();
		struct Exp y = simpleExpression(emitFlag);
		expCombine(op, &x, &y);
	}
	if (emitFlag) emit(")");
	return x;
}

struct Exp
simpleExpression(char emitFlag)
{
	if (emitFlag) emit("(");
	struct Exp x;
	if (token == TOK_PLUS) {
		next();
		x = term(emitFlag);
		checkInt(&x);
	} else if (token == TOK_MINUS) {
		next();
		x = term(emitFlag);
		if (emitFlag) emit("-");
		checkInt(&x);
		expNeg(&x);
	} else {
		x = term(emitFlag);
	}
	while (token >= TOK_PLUS && token <= TOK_OR) {
		int op = token - TOK_TIMES;
		if (emitFlag) emit("%s", opEmit[op]);
		next();
		struct Exp y = term(emitFlag);
		expCombine(op, &x, &y);
	}
	if (emitFlag) emit(")");
	return x;
}

struct Exp
term(char emitFlag)
{
	if (emitFlag) emit("(");
	struct Exp x = factor(emitFlag);
	while (token >= TOK_TIMES && token <= TOK_AND) {
		int op = token - TOK_TIMES;
		if (emitFlag) emit("%s", opEmit[op]);
		next();
		struct Exp y = factor(emitFlag);
		expCombine(op, &x, &y);
	}
	if (emitFlag) emit(")");
	return x;
}

struct Exp
factor(char emitFlag)
{
	struct Exp x;
	struct Object *ob;
	switch (token) {
	case TOK_IDENTIFIER:
		ob = findObject(ident);
		if (ob == NULL) error("expected an object, found '%s'", ident);
		if (emitFlag) {
			if (ob->class == CLASS_PARAMETER && ob->type->form != TYPE_RECORD)
				emit("*");
			emit("%s", ident);
		}
		next();
		x = selector(emitFlag, ob);
		break;
	case TOK_INTEGER:
		x.class = CLASS_CONSTANT;
		x.type = baseInt;
		x.value = val;
		if (emitFlag) emit("%i", val);
		next();
		break;
	case TOK_FALSE:
	case TOK_TRUE:
		x.class = CLASS_CONSTANT;
		x.type = baseBool;
		x.value = val;
		if (emitFlag) emit("%i", val);
		next();
		break;
	case TOK_LEFTPAREN:
		if (emitFlag) emit("(");
		next();
		x = expression(emitFlag);
		expect(TOK_RIGHTPAREN);
		if (emitFlag) emit(")");
		break;
	case TOK_NOT:
		next();
		if (emitFlag) emit("!");
		x = factor(emitFlag);
		if (x.type != baseBool) error("expected boolean type");
		expNeg(&x);
		break;
	default:
		error("expected a factor, got %s", tokens[token]); 
		break;
	}
	return x;
}

struct Exp
selector(char emitFlag, struct Object *start)
{
	struct Object *current;
	struct Exp x = {
		start->type,
		start->val,
		start->class
	};
	while (1) {
		switch (token) {
		case TOK_PERIOD:
			next();
			expectIdentifier();
			if (start->type == NULL || start->type->form != TYPE_RECORD)
				error("expected record type");
			current = start->type->fields;
			while (current != NULL) {
				if (strcmp(current->name, ident) == 0) break; // shouldn't leave case
				current = current->previous;
			}
			if (current == NULL) error("field '%s' does not exist", ident);
			if (emitFlag) {
				if (start->class == CLASS_PARAMETER) emit("->");
				else emit(".");
				emit("%s", current->name);
			}
			x.type = current->type;
			start = current;
			break;
		case TOK_LEFTBRACKET:
			next();
			if (start->type == NULL || start->type->form != TYPE_ARRAY)
				error("expected array type");
			emit("[");
			x = expression(emitFlag); // must emit either way
			expect(TOK_RIGHTBRACKET);
			emit("]");
			x.type = start->type->base;
			start = current;
			break;
		default: return x; break;
		}
	}
	return x;
}

void
identList(struct Object **list, int class)
{
	// far as I'm aware this is ONLY for vars
	newObjectAttached(list, ident, class);
	(*list)->type = NULL;
	expectIdentifier();
	while (token == TOK_COMMA) {
		next();
		// it will fast fail on no identifier
		newObjectAttached(list, ident, CLASS_VARIABLE);
		expectIdentifier();
	}
}

struct Type *
type()
{
	struct Type *t = NULL;
	struct Exp e;
	struct Object *obj;
	switch (token) {
	case TOK_IDENTIFIER:
		// now check if this identifier really is a type
		if ((obj = findObject(ident)) != NULL) {
			if (obj->class != CLASS_TYPE) error("'%s' is not a type", ident);
			t = obj->type;
		} else error("unregistered type '%s'", ident);
		next();
		break;
	case TOK_ARRAY: // ArrayType
		next();
		e = expression(0);
		if (e.class != CLASS_CONSTANT || e.type != baseInt)
			error("array length must be constant integer");
		expect(TOK_OF);
		t = type();
		addType(TYPE_ARRAY, NULL, e.value, t);
		t = typeRoot->type;
		break;
	case TOK_RECORD: // RecordType
		next();
		struct Object *fields = fieldList();
		while (token == TOK_SEMICOLON) {
			next();
			struct Object *fields2 = fieldList();
			struct Object *current = fields;
			if (fields2 != NULL)
				while (current != NULL && current->previous != NULL)
					current = current->previous;
			current->previous = fields2; // append all field lists together
		}
		addType(TYPE_RECORD, fields, 0, NULL);
		t = typeRoot->type;
		expect(TOK_END);
		break;
	default: 
		error("expected type");
		break;
	}
	return t;
}

struct Object *
fieldList()
{
	if (token == TOK_IDENTIFIER) {
		struct Object *objects = NULL;
		// redo of identList for fieldList
		newField(&objects, ident, CLASS_VARIABLE);
		expectIdentifier();
		while (token == TOK_COMMA) {
			next();
			// it will fast fail on no identifier
			newField(&objects, ident, CLASS_VARIABLE);
		}
		expect(TOK_COLON);
		struct Type *t = type();
		struct Object *current = objects;
		while (current != NULL) {
			current->type = t;
			current = current->previous;
		}
		return objects;
	}
	return NULL;
}

struct Object *
fpSection()
{
	int class = CLASS_CONST_PARAM;
	if (token == TOK_VARIABLE) { class = CLASS_PARAMETER; next(); }
	struct Object *obj = NULL;
	identList(&obj, class);
	expect(TOK_COLON);
	struct Type *t = type();
	struct Object *current = obj;
	while (current != NULL) {
		current->type = t;
		current = current->previous;
	}
	return obj;
}

void
statementSequence()
{
	statement();
	while (token == TOK_SEMICOLON) {
		next();
		statement();
	}
}

void
statement()
{	
	struct Object *o;
	struct Exp y;
	switch (token) {
	case TOK_IDENTIFIER: // assignment or ProcedureCall
		o = findObject(ident);
		if (o == NULL) error("could not find object '%s'", ident);
		if (o->class == CLASS_PARAMETER && (o->type->form == TYPE_INTEGER 
			|| o->type->form == TYPE_BOOLEAN)) emit("*");
		emit("%s", ident);
		next();
		y = selector(1, o);
		if (token == TOK_ASSIGN) { // assignment
			if (o->class != CLASS_VARIABLE && o->class != CLASS_PARAMETER)
				error("assignment expects variables/parameters");
			next();
			emit("=");
			struct Exp x = expression(1);
			if (x.type != y.type) error("incompatible types in assignment");
		} else if (token == TOK_LEFTPAREN) { // ProcedureCall
			if (o->class != CLASS_PROCEDURE) error("expected procedure call");
			struct Object *current = o->params;
			emit("(");
			next();
			// SimpleExpression/factor check
			if (token == TOK_PLUS || token == TOK_MINUS || token == TOK_IDENTIFIER
			  || token >= TOK_INTEGER || token == TOK_LEFTPAREN || token == TOK_NOT) {
			  	if (current == NULL || current->class < CLASS_PARAMETER)
			  		error("too many parameters");
			  	if (token == TOK_IDENTIFIER) {
			  		if (current->class == CLASS_PARAMETER) emit("&");
			  	}
				struct Exp x = expression(1);
				if (x.class == CLASS_CONSTANT && current->class == CLASS_PARAMETER)
					error("cannot modify constant parameter");
				if (current->type != x.type) error("parameter type mismatch");
			  	current = current->previous;
				while (token == TOK_COMMA) {
					emit(",");
			  		if (current == NULL || current->class != CLASS_PARAMETER)
			  			error("too many parameters");
					next();
					if (token == TOK_IDENTIFIER) emit("&");
					x = expression(1);
					if (x.class == CLASS_CONSTANT && current->class == CLASS_PARAMETER)
						error("cannot modify constant parameter");
					if (current->type != x.type) error("parameter type mismatch");
					current = current->previous;
				}
			}
			if (current != NULL && current->class == CLASS_PARAMETER)
				error("too few parameters");
			expect(TOK_RIGHTPAREN);
			emit(")");
		}
		break;
	case TOK_IF: // IfStatement
		next();
		emit("if(");
		expression(1);
		emit("){");
		expect(TOK_THEN);
		statementSequence();
		while (token == TOK_ELSIF) {
			next();
			emit("}else if(");
			expression(1);
			emit("){");
			expect(TOK_THEN);
			statementSequence();
		}
		if (token == TOK_ELSE) {
			emit("}else{");
			next();
			statementSequence();
		}
		emit("}");
		expect(TOK_END);
		break;
	case TOK_WHILE: // While Statement
		next();
		emit("while(");
		expression(1);
		emit("){");
		expect(TOK_DO);
		statementSequence();
		emit("}");
		expect(TOK_END);
		break;
	case TOK_REPEAT: // Repeat Statement
		// this is in the keywords and grammar but not in here where it should be
		emit("do{");
		next();
		statementSequence();
		expect(TOK_UNTIL);
		emit("}while(");
		expression(1);
		emit(")");
		break;
	default: return; break;
	}
	emit(";");
}

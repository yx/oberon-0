#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "scan.h"

static char current;

// extern for parser
char ident[ID_LENGTH];
int val;
size_t line = 1; // get line of parser

char *tokens[] = {"Null", "*", "DIV", "MOD", "&", "+", "-", "OR", "=", "#",
  "<", "<=", ">", ">=", ".", ",", ":", ")", "]", "(", "[", "~", ":=", ";",
  "OF", "THEN", "DO", "UNTIL", "END", "ELSE", "ELSIF", "IF", "WHILE", "REPEAT",
  "ARRAY", "RECORD", "CONST", "TYPE", "VAR", "PROCEDURE", "BEGIN", "MODULE", 
  "Identifier", "Integer"};

void
readIntoIdent() // read identifier
{	
	int i = 0;
	do {
		ident[i] = current;
		i++;
		if (i == ID_LENGTH - 1) break;
	} while (isalnum((current = fgetc(source))));
	ident[i] = '\0'; // terminate identifiers
	ungetc(current, source);
}

int
scanToken()
{
	current = fgetc(source);
	while (isblank(current) || current == '\n') {
		line += (current == '\n'); // count newlines
		current = fgetc(source); 
	}
	if (isalnum(current)) { // it's an identifier or number
		readIntoIdent();
		if (!strcmp(ident, "DIV")) return TOK_DIV;
		else if (!strcmp(ident, "MOD")) return TOK_MOD;
		else if (!strcmp(ident, "OR")) return TOK_OR;
		else if (!strcmp(ident, "OF")) return TOK_OF;
		else if (!strcmp(ident, "THEN")) return TOK_THEN;
		else if (!strcmp(ident, "DO")) return TOK_DO;
		else if (!strcmp(ident, "UNTIL")) return TOK_UNTIL;
		else if (!strcmp(ident, "END")) return TOK_END;
		else if (!strcmp(ident, "ELSE")) return TOK_ELSE;
		else if (!strcmp(ident, "ELSIF")) return TOK_ELSIF;
		else if (!strcmp(ident, "IF")) return TOK_IF;
		else if (!strcmp(ident, "WHILE")) return TOK_WHILE;
		else if (!strcmp(ident, "REPEAT")) return TOK_REPEAT;
		else if (!strcmp(ident, "ARRAY")) return TOK_ARRAY;
		else if (!strcmp(ident, "RECORD")) return TOK_RECORD;
		else if (!strcmp(ident, "CONST")) return TOK_CONSTANT;
		else if (!strcmp(ident, "TYPE")) return TOK_TYPE;
		else if (!strcmp(ident, "VAR")) return TOK_VARIABLE;
		else if (!strcmp(ident, "PROCEDURE")) return TOK_PROCEDURE;
		else if (!strcmp(ident, "BEGIN")) return TOK_BEGIN;
		else if (!strcmp(ident, "MODULE")) return TOK_MODULE;
		else if (!strcmp(ident, "FALSE")) {
			val = 0;
			return TOK_FALSE;
		} else if (!strcmp(ident, "TRUE")) { 
			val = 1;
			return TOK_TRUE;
		}
		else if (isdigit(ident[0])){
			for (int i = 1; ident[i] != '\0'; i++) {
				if (isalpha(ident[i])) return TOK_NULL;
			}
			val = atoi(ident);
			return TOK_INTEGER;
		} else return TOK_IDENTIFIER;
	}
	switch (current) {
	// single character tokens
	case '*': return TOK_TIMES; break;
	case '&': return TOK_AND; break;
	case '+': return TOK_PLUS; break;
	case '-': return TOK_MINUS; break;
	case '=': return TOK_EQUAL; break;
	case '#': return TOK_NOTEQUAL; break;
	case '<': 
		if ((current = fgetc(source)) == '=') return TOK_LESSTHANEQUAL;
		ungetc(current, source);
		return TOK_LESSTHAN; break;
	case '>': 
		if ((current = fgetc(source)) == '=') return TOK_GREATERTHANEQUAL;
		ungetc(current, source);
		return TOK_GREATERTHAN; break;
	case '.': return TOK_PERIOD; break;
	case ',': return TOK_COMMA; break;
	case ':': 
		if ((current = fgetc(source)) == '=') return TOK_ASSIGN;
		ungetc(current, source);
		return TOK_COLON; break;
	case ')': return TOK_RIGHTPAREN; break;
	case ']': return TOK_RIGHTBRACKET; break;
	case '(': return TOK_LEFTPAREN; break;
	case '[': return TOK_LEFTBRACKET; break;
	case '~': return TOK_NOT; break;
	case ';': return TOK_SEMICOLON; break;
	default: // by default we don't know this token
		return TOK_NULL; break;
	}
}

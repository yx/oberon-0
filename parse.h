#pragma once

void parse();

// parse helpers
void module();
void declarations();
struct Exp expression(char); // are we going to emit code?
struct Exp simpleExpression(char);
struct Exp term(char);
struct Exp factor(char);
struct Exp selector(char, struct Object *);
struct Type *type();
struct Object *fieldList();
void identList(struct Object **, int class);
struct Object *fpSection();
void statementSequence();
void statement();

#include <stdio.h>
#include <stdlib.h>

#include "cg.h"
#include "parse.h"
#include "store.h"

FILE *source;

void
cleanup()
{
	destroyAllScopes();
	destroyTypeCollection();
	fclose(source);
	if (outc != NULL) fclose(outc);
}

int
main(int argc, char *argv[])
{
	if (argc != 2) {
		printf("usage: %s [file]\n", argv[0]);
		exit(1);
	}
	setupOutput("out.c");

	source = fopen(argv[1], "r");
	if (source == NULL) {
		printf("could not open file %s\n", argv[1]);
		exit(1);
	}
	atexit(cleanup);
	initTopScope();

	parse();
	printf("%s is valid Oberon-0 grammar\n", argv[1]);

	printf("objects created:\n");
	printScopes();

	return 0;
}

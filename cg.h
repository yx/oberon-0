#pragma once

// code generation module

extern FILE *outc;

void setupOutput(const char *);

void emit(const char *, ...);
void emitType(struct Type *);

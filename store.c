#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "error.h"
#include "store.h"

struct Scope *lastScope;

struct TypeCollection *typeRoot = NULL;
struct Type *baseInt, *baseBool;

void
opTimes(struct Exp *x, struct Exp *y)
{
	if (x->class == CLASS_CONSTANT) {
		if (y->class == CLASS_CONSTANT) {
			if (x->type != baseInt || y->type != baseInt) {
				error("expected integer types for multiplication");
			}
			x->value *= y->value;
		} else {
			x->class = CLASS_VARIABLE;
		}
	}
}

void
opDiv(struct Exp *x, struct Exp *y)
{
	if (x->class == CLASS_CONSTANT) {
		if (y->class == CLASS_CONSTANT) {
			if (x->type != baseInt || y->type != baseInt) {
				error("expected integer types for division");
			}
			x->value /= y->value;
		} else {
			x->class = CLASS_VARIABLE;
		}
	}
}

void
opMod(struct Exp *x, struct Exp *y)
{
	if (x->class == CLASS_CONSTANT) {
		if (y->class == CLASS_CONSTANT) {
			if (x->type != baseInt || y->type != baseInt) {
				error("expected integer types for modulus");
			}
			x->value %= y->value;
		} else {
			x->class = CLASS_VARIABLE;
		}
	}
}

void
opAnd(struct Exp *x, struct Exp *y)
{
	if (x->class == CLASS_CONSTANT) {
		if (y->class == CLASS_CONSTANT) {
			if (x->type != baseBool || y->type != baseBool) {
				error("expected boolean types for logical and");
			}
			x->value &= y->value;
		} else {
			x->class = CLASS_VARIABLE;
		}
	}
}

void
opPlus(struct Exp *x, struct Exp *y)
{
	if (x->class == CLASS_CONSTANT) {
		if (y->class == CLASS_CONSTANT) {
			if (x->type != baseInt || y->type != baseInt) {
				error("expected integer types for addition");
			}
			x->value += y->value;
		} else {
			x->class = CLASS_VARIABLE;
		}
	}
}

void
opMinus(struct Exp *x, struct Exp *y)
{
	if (x->class == CLASS_CONSTANT) {
		if (y->class == CLASS_CONSTANT) {
			if (x->type != baseInt || y->type != baseInt) {
				error("expected integer types for subtraction");
			}
			x->value -= y->value;
		} else {
			x->class = CLASS_VARIABLE;
		}
	}
}

void
opOr(struct Exp *x, struct Exp *y)
{
	if (x->class == CLASS_CONSTANT) {
		if (y->class == CLASS_CONSTANT) {
			if (x->type != baseBool || y->type != baseBool) {
				error("expected boolean types for logical or");
			}
			x->value |= y->value;
		} else {
			x->class = CLASS_VARIABLE;
		}
	}
}

void
opEqual(struct Exp *x, struct Exp *y)
{
	if (x->class == CLASS_CONSTANT) {
		if (y->class == CLASS_CONSTANT) {
			if (x->type != baseInt || y->type != baseInt) {
				error("expected integer types for equality checks");
			}
			x->value = (x->value == y->value);
		} else {
			x->class = CLASS_VARIABLE;
		}
	}
	x->type = baseBool;
}

void
opNotEqual(struct Exp *x, struct Exp *y)
{
	if (x->class == CLASS_CONSTANT) {
		if (y->class == CLASS_CONSTANT) {
			if (x->type != baseInt || y->type != baseInt) {
				error("expected integer types for equality checks");
			}
			x->value = (x->value != y->value);
		} else {
			x->class = CLASS_VARIABLE;
		}
	}
	x->type = baseBool;
}

void
opLessThan(struct Exp *x, struct Exp *y)
{
	if (x->class == CLASS_CONSTANT) {
		if (y->class == CLASS_CONSTANT) {
			if (x->type != baseInt || y->type != baseInt) {
				error("expected integer types for equality checks");
			}
			x->value = (x->value < y->value);
		} else {
			x->class = CLASS_VARIABLE;
		}
	}
	x->type = baseBool;
}

void
opLessThanEqual(struct Exp *x, struct Exp *y)
{
	if (x->class == CLASS_CONSTANT) {
		if (y->class == CLASS_CONSTANT) {
			if (x->type != baseInt || y->type != baseInt) {
				error("expected integer types for equality checks");
			}
			x->value = (x->value <= y->value);
		} else {
			x->class = CLASS_VARIABLE;
		}
	}
	x->type = baseBool;
}

void
opGreaterThan(struct Exp *x, struct Exp *y)
{
	if (x->class == CLASS_CONSTANT) {
		if (y->class == CLASS_CONSTANT) {
			if (x->type != baseInt || y->type != baseInt) {
				error("expected integer types for equality checks");
			}
			x->value = (x->value > y->value);
		} else {
			x->class = CLASS_VARIABLE;
		}
	}
	x->type = baseBool;
}

void
opGreaterThanEqual(struct Exp *x, struct Exp *y)
{
	if (x->class == CLASS_CONSTANT) {
		if (y->class == CLASS_CONSTANT) {
			if (x->type != baseInt || y->type != baseInt) {
				error("expected integer types for equality checks");
			}
			x->value = (x->value >= y->value);
		} else {
			x->class = CLASS_VARIABLE;
		}
	}
	x->type = baseBool;
}

void (*ops[])(struct Exp *, struct Exp *) = { opTimes, opDiv, opMod, opAnd, 
	opPlus, opMinus, opOr, opEqual, opNotEqual, opLessThan, opLessThanEqual,
	opGreaterThan, opGreaterThanEqual };

void
expNeg(struct Exp *e)
{
	if (e->class = CLASS_CONSTANT) e->value = !e->value;
}

void
expCombine(int op, struct Exp *x, struct Exp *y)
{
	ops[op](x, y);
}

void
destroyAllScopes()
{
	struct Scope *next;
	while (lastScope != NULL) {
		next = lastScope->previous;
		destroyScope();
		lastScope = next;
	}
}

void
destroyScope()
{
	struct Object *current = lastScope->last, *next;
	while (current != NULL) {
		next = current->previous;
		free(current);
		current = next;
	}
	struct Scope *scope = lastScope->previous;
	free(lastScope);
	lastScope = scope;
}

void
newScope(const char name[ID_LENGTH])
{
	struct Scope *new = malloc(sizeof(struct Scope));
	if (new == NULL) error("scope allocation failed");
	memcpy(new->name, name, sizeof(char) * ID_LENGTH);
	new->last = NULL;
	new->previous = lastScope;
	lastScope = new;
}

void
newObjectAttached(struct Object **obj, const char name[ID_LENGTH], int class)
{
	struct Object *new = malloc(sizeof(struct Object));
	if (new == NULL) error("object allocation failed");
	// I could choose to allow shadowing but this is more likely in my own project
	// any duplicate names error
	if (findObject(name) != NULL)
		error("object '%s' already exists", name);
	// our entry is fine
	memcpy(new->name, name, sizeof(char) * ID_LENGTH);
	new->class = class;
	new->type = NULL;
	// other fields will be modified as needed
	new->previous = *obj;
	*obj = new;
}

// adding fields needs to be handled seperately
void
newField(struct Object **obj, const char name[ID_LENGTH], int class)
{
	struct Object *new = malloc(sizeof(struct Object));
	if (new == NULL) error("field allocation failed");
	struct Object *current = *obj;
	while (current != NULL) {
		if (strcmp(name, current->name) != 0)
			error("field collision '%s'", name);
		current = current->previous;
	}
	memcpy(new->name, name, sizeof(char) * ID_LENGTH);
	new->class = class;
	new->type = NULL;
	new->previous = *obj;
	*obj = new;
}

void
newObject(const char name[ID_LENGTH], int class)
{
	newObjectAttached(&lastScope->last, name, class);
}

struct Object *
findObject(const char *name)
{
	struct Scope *currentScope = lastScope;
	while (currentScope != NULL) {
		struct Object *current = currentScope->last;
		while (current != NULL) {
			if (current->class == CLASS_PROCEDURE) {
				struct Object *c = current->params;
				while (c != NULL) {
					if (!strcmp(name, c->name)) return c;
					c = c->previous;
				}
			}
			if (!strcmp(name, current->name)) return current;
			current = current->previous;
		}
		currentScope = currentScope->previous;
	}
	return NULL;
}

void
initTopScope()
{
	lastScope = NULL;
	newScope("~TopScope");
	newObject("INTEGER", CLASS_TYPE);
	addType(TYPE_INTEGER, NULL, 0, NULL);
	baseInt = typeRoot->type;
	lastScope->last->type = baseInt;
	newObject("BOOLEAN", CLASS_TYPE);
	addType(TYPE_BOOLEAN, NULL, 0, NULL);
	baseBool = typeRoot->type;
	lastScope->last->type = baseBool;

	newObject("WriteInt", CLASS_PROCEDURE);
	newObjectAttached(&lastScope->last->params, "N", CLASS_CONST_PARAM);
	lastScope->last->params->type = baseInt;
	newScope("WriteInt");
}

void
printType(struct Type *type)
{
	if (type->form == TYPE_ARRAY) {
		printf("ARRAY %i OF ", type->len);
		printType(type->base);
	} else if (type->form == TYPE_RECORD) {
		printf("RECORD WITH");
		struct Object *current = type->fields;
		while (current != NULL) {
			printf("\n    %s: ", current->name);
			printType(current->type);
			current = current->previous;
		}
	} else if (type->form == TYPE_INTEGER) {
		printf("INTEGER ");
	} else { // Boolean
		printf("BOOLEAN ");
	}
}

void
printObject(struct Object *object)
{
	if (object->class == CLASS_CONSTANT) {
		printf("CONST %s OF ", object->name);
		printType(object->type);
		printf("IS %i\n", object->val);
	} else if (object->class == CLASS_TYPE) {
		printf("TYPE %s IS ", object->name);
		printType(object->type);
		printf("\n");
	} else if (object->class == CLASS_VARIABLE) {
		printf("VAR %s: ", object->name);
		printType(object->type);
		printf("\n");
	} else if (object->class == CLASS_PARAMETER) {
		printf("PARAM %s: ", object->name);
		printType(object->type);
		printf("\n");
	} else { // Procedure
		printf("PROCEDURE %s\n", object->name);
	}
}

void
printScopes()
{
	struct Scope *scope = lastScope;
	while (scope != NULL) {
		printf("%s\n", scope->name);
		struct Object *object = scope->last;
		while (object != NULL) {
			printf("  ");
			printObject(object);
			object = object->previous;
		}
		scope = scope->previous;
	}
}

void
addType(int form, struct Object *fields, int len, struct Type *base)
{
	struct TypeCollection *new = malloc(sizeof(struct TypeCollection));
	if (new == NULL) error("could not allocate new type");
	struct Type *newType = malloc(sizeof(struct Type));
	if (newType == NULL) error("could not allocate new type");
	newType->form = form;
	newType->fields = fields;
	newType->len = len;
	newType->base = base;
	new->type = newType;
	new->type->fields = fields;
	new->previous = typeRoot;
	typeRoot = new;
}

void
destroyTypeCollection()
{
	struct TypeCollection *next = NULL;
	while (typeRoot != NULL) {
		next = typeRoot->previous;
		struct Object *current = typeRoot->type->fields;
		while (current != NULL) {
			struct Object *next = current->previous;
			free(current);
			current = next;
		}
		// don't need to free base types, they all live in this list
		free(typeRoot->type);
		free(typeRoot);
		typeRoot = next;
	}
}

CC := tcc
CFLAGS := -Wall -MD

OUT := ob0c

SRC != find *.c | grep -v "out.c"
OBJ := $(SRC:.c=.o)
DEP := $(SRC:.c=.d)

.SUFFIXES: .c .o

.c.o:
	$(CC) $< -c -o $@ $(CFLAGS)

$(OUT): $(OBJ)
	$(CC) $(OBJ) -o $(OUT) 

.PHONY: clean

clean:
	rm -f $(OUT) $(OBJ) $(DEP)

-include $(DEP)
